package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClieTelDTO implements Serializable{

    private String tel;
    private int type;

}