package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CurpValidateByExternalData implements Serializable {

    private String curp;

}