package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidEmprDTO implements Serializable {

    private Long idEmpr;
    private String empr;
    private String username;

}