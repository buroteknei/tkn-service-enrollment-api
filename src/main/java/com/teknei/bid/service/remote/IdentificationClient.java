package com.teknei.bid.service.remote;

import com.teknei.bid.dto.*;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "${tkn.feign.identification-name}")
public interface IdentificationClient {

    @RequestMapping(value = "/identification/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> updateCredentialData(@PathVariable("id") Long id, @RequestBody IneDetailDTO detailDTO);

    @RequestMapping(value = "/identification/download", method = RequestMethod.POST)
    ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto);

    @RequestMapping(value = "/identification/findDetail/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<IneDetailDTO> findDetail(@PathVariable("id") Long id);

    @RequestMapping(value = "/identification/verify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> verifyAgainstINE(@RequestBody PersonDataIneTKNRequestService personData);

    @RequestMapping(value = "/identification/uploadAdditionalPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> uploadAdditionalCredentials(@RequestBody RequestEncFilesDTO dto);

    @RequestMapping(value = "/identification/uploadCapturePlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> uploadCaptureCredentials(@RequestBody RequestEncFilesDTO dto);

    @RequestMapping(value = "/identification/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> uploadCredentialsAsync(@RequestBody RequestEncFilesDTO dto);

    @RequestMapping(value = "/identification/curp/obtain", method = RequestMethod.POST)
    ResponseEntity<String> getCurp(@RequestBody CurpRequestDTO curpRequestDTO);

    @RequestMapping(value = "/identification/curp/validate", method = RequestMethod.POST)
    ResponseEntity<String> validateCurp(@RequestBody CurpRequestDTO curpRequestDTO);

    @RequestMapping(value = "/identification/rollback/identificationCapture", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> rollbackConfirmationCredentialCapture(@RequestBody BasicRequestDTO basicRequestDTO);
}