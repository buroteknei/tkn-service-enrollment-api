package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentContractCertController;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/contractCert")
@CrossOrigin
public class EnrollmentContractCertControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentContractCertControllerUnauth.class);
    @Autowired
    private EnrollmentContractCertController controller;

    @ApiResponses({
            @ApiResponse(code = 403, message = "The certificate is not present for the current user"),
            @ApiResponse(code = 200, message = "The certificate is present and is valid")
    })
    @ApiOperation(value = "Generates the contract depending on whether the user cert is present or not")
    @RequestMapping(value = "/certContract/{idCustomer}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getContractWithCert(@PathVariable Long idCustomer, HttpServletRequest request){
    	log.info("lblancas: "+this.getClass().getName()+".{getContractWithCert(GET) }");
        return controller.getContractWithCert(idCustomer, request);
    }

}
