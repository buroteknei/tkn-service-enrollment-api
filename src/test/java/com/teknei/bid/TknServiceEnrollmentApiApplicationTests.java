package com.teknei.bid;

import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.Rules;
import com.teknei.bid.controller.rest.util.crypto.Util;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TknServiceEnrollmentApiApplicationTests {

    //@Autowired
    private Decrypt decrypt;
    private static final Logger log = LoggerFactory.getLogger(TknServiceEnrollmentApiApplicationTests.class);
    private static final String PUBLIC_URL = "/opt/bid/crypto/k_pub.key";
    private static final String RIGHT_INDEX = "";

    //@Test
    public void contextLoads() {
        JSONObject responseInner = new JSONObject();
        JSONObject minutiaeResponse = new JSONObject();
        minutiaeResponse.put("similitud7", "75");
        minutiaeResponse.put("similitud2", "75");
        responseInner.put("fechaHoraPeticion", LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE));
        responseInner.put("peticionId", UUID.randomUUID().toString());
        responseInner.put("tiempoProcesamiento", "1000");
        responseInner.put("indiceSolicitud", "300");
        responseInner.put("minutiaeResponse", minutiaeResponse);

        JSONObject response = new JSONObject();
        response.put("response", responseInner);

        JSONObject respCompInner = new JSONObject();
        respCompInner.put("anioRegistro", "1990");
        respCompInner.put("claveElector", "NA");
        respCompInner.put("numeroEmisionCredencial", "123");
        respCompInner.put("curp", "AACJ881203HMCMRR08");
        respCompInner.put("apellidoPaterno", "AMARO");
        respCompInner.put("apellidoMaterno", "CORIA");
        respCompInner.put("ocr", "NA");
        JSONObject respComp = new JSONObject();

        JSONObject respRegisInner = new JSONObject();
        respRegisInner.put("tipoSituacionRegistral", "NA");

        JSONObject respRegis = new JSONObject();
        respRegis.put("respuestaSituacionRegistral", respRegisInner);

        JSONObject dataRespInner = new JSONObject();
        dataRespInner.put("respuestaComparacion", respCompInner);
        dataRespInner.put("respuestaSituacionRegistral", respRegis);

        response.put("dataResponse", dataRespInner);
    }

    //@Test
    public void testCypherDecypher() throws IOException {
        String url = "/home/amaro/clave.txt";
        String content = Files.readAllLines(Paths.get(url)).get(0);
        String text = content;
        byte[] primaryBytes = text.getBytes();
        Rules rules = new Rules();
        Util util = new Util();
        util.emptyBuffer();
        util.setLenBuffer(primaryBytes.length + 3);
        util.addBuffer(primaryBytes);
        byte[] target = util.getBuffer().array();
        byte[] cyphered = rules.generate(target, "112233", "3", "1", PUBLIC_URL);
        String b64Cyphered = Base64Utils.encodeToString(cyphered);
        log.info("Cyphered value: {}", b64Cyphered);
        String clear = decrypt.decrypt(b64Cyphered);
        String b64Clear = new String(Base64Utils.decodeFromString(clear));
        log.info("Source: {} Target: {}, New: {}", text, clear, b64Clear);
        assertEquals(text, b64Clear);
    }

    //@Test
    public void testEquality() throws IOException {
        String url = "/home/amaro/clave.txt";
        String urlClear = "/home/amaro/claro.txt";
        String clear = Files.readAllLines(Paths.get(urlClear)).get(0);
        String text = Files.readAllLines(Paths.get(url)).get(0);
        String b64Clear = decrypt.decrypt(text);
        String realClear = new String(Base64Utils.decodeFromString(b64Clear));
        assertEquals(realClear, clear);
    }

}
